import fetch from 'node-fetch';

import jsrsasign from 'jsrsasign'

const root_server_url = 'http://' + process.env.API_SERVER_IP + ':3000';
const root_authentication_url = 'http://' + process.env.AUTHENTICATION_SERVER_IP + ':8000';

// TODO: add request to this data structure, might be useful for debugging.
export class ApiResponse {
    constructor(fetchResponse, data, error = null) {
        this.fetchResponse = fetchResponse
        // Usually so API manager can parse the json.
        // data can contain error or result, or be null.
        this.data = data;
        // Used if an internal error occurred (should not be an answer from server)
        this.error = error
    }
}

export class TruthLieServerApiManager {

    constructor () {
        // calling super gives you access to the subModules object
        this._userId = 0;
        this._userName = "";
        this._apiToken = "";
    }
    getUserId() {
        return this._userId;
    }
    getUserName() {
        return this._userName;
    }
    isAuthenticated() {
        // TODO: check apiToken is still valid (careful to expiration date)
        return this._userId != 0 && this._userName != "" && this._apiToken != "";
    }
    async setUserName(userName) {

        this._userName = userName;
        let request = {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            },
            body: JSON.stringify({
                player_name: this.getUserName()
            })
        };
        try {
            let response = await fetch(root_server_url + "/players/name/set", request);
            if (response.ok) {
                return new ApiResponse(response, null);
            }
            else {
                // TODO: we could store the fact the username was not really updated and try again later.
                return new ApiResponse(null, null, "unknown error");
            }
        } catch(error) {
            console.log("Error: " + JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }
    /**
     * @param {callback} callback - The callback called after request to server, send error as parameter
     */
    async login(facebookUserId, facebookAccessToken, callback) {
        let request = {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user_id: facebookUserId,
                // TODO: send real access token
                access_token: facebookAccessToken,
                issuer: "Facebook"
            })
        };
        console.log(JSON.stringify(request));
        console.log("authentication: " + root_authentication_url);
        try {
            let response = await fetch(root_authentication_url + "/authorize", request);
            let responseJson = await response.json();
            let a = responseJson.split(".");
            console.log("(token): responseJson: " + responseJson);
            let pClaim = jsrsasign.jws.JWS.readSafeJSONString(jsrsasign.b64utoutf8(a[1]));
            
            this._apiToken = responseJson;
            this._userId = parseInt(pClaim.sub);
            console.log(JSON.stringify(pClaim));
            let playerDetails = await this.retrievePlayerDetails();
            if (playerDetails.fetchResponse.ok) {
                console.log("received plyer details: " + JSON.stringify(playerDetails));
                this._userName = playerDetails.data.name;
            }
            callback();
        } catch(error) {
            callback(error);
        }
    }

    async retrievePlayerDetails() {
        let request = {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            }
        };
        try {
            let url = root_server_url + "/players/me";
            let response = await fetch(url, request);
            let responseJson = await response.json();
            if (response.ok) {
                return new ApiResponse(response, responseJson);
            }
            else {
                return new ApiResponse(null, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }

    /**
     * 
     * @param answerToCreate
     * {
     *   is_truth: true,
     *   content: "truth2"
     * }
     * 
     * @returns ApiResponse
     */
    async createAnswer(answerToCreate) {
        let request = {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            },
            body: JSON.stringify({
                player_id: this.getUserId(),
                is_truth: answerToCreate.is_truth,
                content: answerToCreate.content
            })
        };
        try {
            console.log("sending: " + JSON.stringify(request));
            let response = await fetch(root_server_url + "/players/answers/create", request);
            console.log(JSON.stringify(response));
            if (response.ok) {
                return new ApiResponse(response, null);
            }
            else {
                return new ApiResponse(response, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }

    /**
     * 
     * @param cardToSend 
     * {
     *   facebookFriends:["id1", "id2"], 
     *   truth1:"truth1",
     *   truth2:"truth2",
     *   lie:"lie"
     * }
     * 
     * @returns error
     */
    async sendCard3(cardToSend) {
        let request = {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            },
            body: JSON.stringify({
                player_id: this.getUserId(),
                player_target_facebook_ids: cardToSend.facebookFriends,
                answers: [
                    {
                        content: cardToSend.truth1,
                        is_truth: true
                    },
                    {
                        content: cardToSend.truth2,
                        is_truth: true
                    },
                    {
                        content: cardToSend.lie,
                        is_truth: false
                    }
                ]
            })
        };
        try {
            let response = await fetch(root_server_url + "/players/cards/send_card_3", request);
            console.log(JSON.stringify(response));
            if (response.ok) {
                return new ApiResponse(response, null);
            }
            else {
                return new ApiResponse(response, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }

    /**
     * 
     * @param parameters 
     * {
     *   offset:0,
     *   limit:10,
     *   is_answered: false
     * }
     * 
     * @returns received guesses
     */
    async getReceivedGuesses(parameters = {offset:0, limit: 10, is_answered: false}) {
        let request = {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            }
        };
        try {
            let url = root_server_url + "/players/guesses/get?offset=" + parameters.offset + "&limit=" + parameters.limit + "&is_answered=" + parameters.is_answered;
            let response = await fetch(url, request);
            let responseJson = await response.json();
            if (response.ok) {
                return new ApiResponse(response, responseJson);
            }
            else {
                return new ApiResponse(response, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }
    
    /**
     * 
     * @param guess 
     * {
     *   guessId:(int),
     *   answerId:(int)
     * }
     * 
     * @returns result: {is_truth:(bool)}
     */
    async guess(guess) {
        let request = {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            },
            body: JSON.stringify({
                player_id:this.getUserId(),
                guess_id:guess.guessId,
                answer_id:guess.answerId
            })
        };
        try {
            let url = root_server_url + "/players/guesses/guess";
            let response = await fetch(url, request);
            console.log(JSON.stringify(response));
            let responseJson = await response.json();
            if (response.ok) {
                return new ApiResponse(response, responseJson);
            }
            else {
                return new ApiResponse(response, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }

    /**
     * 
     * @param pagination 
     * {
     *   offset:0,
     *   limit:10
     * }
     * 
     * @returns created cards
     */
    async getCreatedCards(pagination = {offset:0, limit: 10}) {
        let request = {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._apiToken
            }
        };
        try {
            let url = root_server_url + "/players/cards/get?offset=" + pagination.offset + "&limit=" + pagination.limit;
            let response = await fetch(url, request);
            let responseJson = await response.json();
            if (response.ok) {
                return new ApiResponse(response, responseJson);
            }
            else {
                return new ApiResponse(response, null, "unknown error");
            }
        } catch(error) {
            console.log(JSON.stringify(error));
            return new ApiResponse(null, null, error);
        }
    }
}

export default new TruthLieServerApiManager();