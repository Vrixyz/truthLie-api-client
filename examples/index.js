// Must be first, other dependencies might depend on process.env
require("dotenv").config();

let client = require("../lib/index.js").default;


let toml = require("toml");
var concat = require("concat-stream");
var fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: "> "
});
rl.on("close", () => {
    console.log("Have a great day!");
    process.exit(0);
});

let fbUserId = null;
let fbAccessToken = null;

let StateEnum = Object.freeze({
    AskCredentials: {
        action: (setNewState) => {
            rl.question("Enter your Facebook User ID (for TruthLie): ", (fbUserId) => {
                console.log(`Facebook User ID: ${fbUserId}`);
                rl.question("Enter your associated Facebook Access Token: ", (fbAccessToken) => {
                    console.log(`Facebook Access Token: ${fbAccessToken}`);
    
                    client.login(fbUserId, fbAccessToken, (error) => {

                        console.log("error ?: " + error);
                        if (error) {
                            setNewState(StateEnum.AskCredentials);
                        }
                        else {
                            setNewState(StateEnum.Home);
                        }
                    }).then((result) => {
                        // TODO: I should use promises instead of dirty callback
                    }).catch((err) => {
                        console.log("error: " + err);
                        setNewState(StateEnum.AskCredentials);
                    });
                });
            });
        }
    },
    Home:{
        action: (setNewState) => {
            rl.question("what do you want to do ? (setup|auto|nothing) ", (input) => {
                if (input.toLowerCase() == "setup") {
                    setNewState(StateEnum.Setup);
                    return;
                }
                else if (input.toLowerCase() == "auto") {
                    setNewState(StateEnum.Auto);
                    return;
                }
                console.log("what ?");
                setNewState(StateEnum.Home);
            });
        }
    },
    Setup:{
        action: (setNewState) => {
            // if not authentified; exit
            if (!client.isAuthenticated()) {
                console.log("You're not authenticated.");
                setNewState(StateEnum.Home);
            }
            // Read answers to create
            fs.createReadStream("examples/data.toml", "utf8").pipe(concat(function(data) {
                let parsed = toml.parse(data);
                console.log("parsed: " + JSON.stringify(parsed));
                
                // TODO: randomize answers creation to avoid deduction of lies/truths (big id will be lies)
                let nextState = undefined;
                parsed.answers.truths.forEach((truth) => {
                    client.createAnswer(
                    {
                        is_truth: true,
                        content: truth
                    }).then((response) => {
                        if (response.error || !response.fetchResponse.ok) {
                            console.log("ERROR sending: " + truth);
                            console.log("ERROR: " + JSON.stringify(response));
                            console.log("");
                            nextState = StateEnum.Home
                            return;
                        }
                        console.log("truth " + truth + " created.")
                    });
                }).then(() => {
                    parsed.answers.lies.forEach((lie) => {
                        client.createAnswer(
                            {
                                is_truth: false,
                                content: lie
                            }).then((response) => {
                                if (response.error || !response.fetchResponse.ok) {
                                    console.log("ERROR sending: " + lie);
                                    console.log("ERROR: " + JSON.stringify(response));
                                    console.log("");
                                    nextState = StateEnum.Home
                                    return;
                                }
                                console.log("lie " + lie + " created.")
                            });
                    }).then(() => {
                        if (nextState != undefined) {
                            // TODO: go to a state where we GET created answers from server, then store it,
                            // then (other state ?) GET our guesses (unanswered?), then store their players (remove duplicates) -> we get players who interacted with us,
                            // then (other state ?) TODO SERVER: GET unanswered guesses we sent to those players, remove their players. (server logic: only keep the players who have no pending guess (= no guesses OR (all or last?) guesses answered)) -> we get players who are waiting for a new guess
                            // then (other state ?) send them a card with random stored answers.
                            nextState = StateEnum.Home;
                        }
                        setNewState(nextState);                    
                    });
                });
            }));
        }
    },
    Auto:{
        action: (setNewState) => {
            setNewState(StateEnum.GetUnansweredGuesses);
        }
    },
    // To know who to send cards to.
    GetUnansweredGuesses: {
        action: (setNewState) => {
            client.getReceivedGuesses(
                {
                    offset:0,
                    limit:1000,
                    is_answered: false
                }
            ).then((result) => {
                console.log("result: " + JSON.stringify(result));
                if (result.fetchResponse.ok && result.error == undefined) {
                    console.log("geunansweredGuesses: " + JSON.stringify(result.data));
                    // TODO: filter unique senders
                    // TODO: iterate on unique senders
                    // - get last guess we sent to them (TODO server)
                    // - if not answered: stop here (maybe send a reminder ?) ; if there are no guesses, continue.
                    // - get a random card not sent to player (TODO server)
                    // - OR get next card to send (get cards with {offset:lastguess.id + 1, limit:1} ; filter by not sent to player.)
                    // - send that card to player. 
                }
                else {
                // TODO: send to askcredentials sometimes ?
                    setNewState(StateEnum.Home);
                }
            }).catch((err) => {
                // TODO: send to askcredentials sometimes ?
                console.log("error: " + err);
                setNewState(StateEnum.Home);
            });
        }
    },
});

let currentState = StateEnum.AskCredentials;

function inputLoop() {
    currentState.action((newState) => {
        currentState = newState;
        inputLoop();
    });
}

inputLoop();